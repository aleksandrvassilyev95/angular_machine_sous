import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-randomiser',
  templateUrl: './randomiser.component.html',
  styleUrls: ['./randomiser.component.css']
})
export class RandomiserComponent {

@Output() sendRequest = new EventEmitter();

pierre = "pierre";
papier = "papier";
ciseaux = "ciseaux";

sendPierre() {
  this.sendRequest.emit(this.pierre);
}

sendPapier() {
  this.sendRequest.emit(this.papier);
}

sendCiseaux() {
  this.sendRequest.emit(this.ciseaux);
}



}
