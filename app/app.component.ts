import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JeuPierrePapierCiseaux';


  random: any;
  final: any;
  showPierre = false;
  showPapier = false;
  showCiseaux = false;
  explication: any;
  item: any;

  checkChoise(choise) {
    //Methode de Random
    this.random = Math.floor(Math.random() * Math.floor(3));


    //Si utilisateur a choisi Pierre
    if (choise == "pierre" && this.random == 0) {
      this.final = "Egalité";
      this.showPierre = true;
      this.showPapier = false;
      this.showCiseaux = false;

    } else if (choise == "pierre" && this.random == 1) {
      this.final = "Perdu...";
      this.showPierre = false;
      this.showPapier = true;
      this.showCiseaux = false;

    } else if (choise == "pierre" && this.random == 2) {
      this.final = "Gagne!";
      this.showPierre = false;
      this.showPapier = false;
      this.showCiseaux = true;

    }
    //Si utilisateur a choisi Papier
    if (choise == "papier" && this.random == 0) {
      this.final = "Gagne!";
      this.showPierre = true;
      this.showPapier = false;
      this.showCiseaux = false;

    } else if (choise == "papier" && this.random == 1) {
      this.final = "Egalité";
      this.showPierre = false;
      this.showPapier = true;
      this.showCiseaux = false;

    } else if (choise == "papier" && this.random == 2) {
      this.final = "Perdu...";
      this.showPierre = false;
      this.showPapier = false;
      this.showCiseaux = true;
    }
    //Si utilisateur a choisi Ciseaux
    if (choise == "ciseaux" && this.random == 0) {
      this.final = "Perdu...";
      this.showPierre = true;
      this.showPapier = false;
      this.showCiseaux = false;

    } else if (choise == "ciseaux" && this.random == 1) {
      this.final = "Gagne!";
      this.showPierre = false;
      this.showPapier = true;
      this.showCiseaux = false;

    } else if (choise == "ciseaux" && this.random == 2) {
      this.final = "Egalité";
      this.showPierre = false;
      this.showPapier = false;
      this.showCiseaux = true;
    }

    if (this.random == 0) {
      this.item = "pierre";
    } else if (this.random == 1) {
      this.item = "papier";
    } else if (this.random == 2) {
      this.item = "siceaux";
    }



    this.explication = "Vous avez choisi " + choise + " et votre amie a choisi " + this.item + " donc vous avez " + this.final;

  }

}



